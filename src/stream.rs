use {
    std::{
        collections::{VecDeque, BTreeMap},
        sync::{Arc, Weak},
    },

    bytes::Bytes,
    futures::channel::oneshot,
    smallvec::SmallVec,
    tokio::sync::{Mutex, RwLock}
};

// type ChannelName = String;
// 
// struct Rendition {
// }
// 
// struct Rendition {
//     vertical_resolution: u32,
//     playlist: String,
//     playing: Option<Session>,
// }
//

type Waiting = BTreeMap<u64, SmallVec<[oneshot::Sender<()>; 4]>>;

pub struct Tx {
    splitter: MjpegSplitter,

    buffer: Arc<RwLock<Buffer>>,
    waiting: Weak<Mutex<Waiting>>,
}

pub struct Rx {
    buffer: Weak<RwLock<Buffer>>,
    waiting: Arc<Mutex<Waiting>>,

    _client_count: Arc<()>,
}

pub struct Fork {
    buffer: Weak<RwLock<Buffer>>,
    waiting: Weak<Mutex<Waiting>>,

    client_count: Arc<()>,
}

impl Fork {
    pub fn get_rx(&self) -> Option<Rx> {
        Some(Rx {
            buffer: self.buffer.clone(),
            waiting: self.waiting.upgrade()?,

            _client_count: self.client_count.clone(),
        })
    }

    pub fn get_count(&self) -> usize {
        Arc::strong_count(&self.client_count) - 1
    }
}

pub fn new() -> (Tx, Rx, Fork) {
    let buffer = Arc::new(RwLock::new(Buffer::new()));
    let waiting = Arc::new(Mutex::new(Waiting::new()));

    let buffer_weak = Arc::downgrade(&buffer);
    let waiting_weak = Arc::downgrade(&waiting);

    let fork = Fork {
        buffer: buffer_weak.clone(),
        waiting: waiting_weak.clone(),

        client_count: Arc::new(()),
    };

    let tx = Tx {
        splitter: MjpegSplitter::new(),
        buffer: buffer.clone(),
        waiting: waiting_weak,
    };

    let rx = Rx {
        buffer: buffer_weak,
        waiting: waiting,

        _client_count: fork.client_count.clone(),
    };

    (tx, rx, fork)
}


impl Tx {
   pub async fn write(&mut self, buf: &[u8]) -> bool {
        self.splitter.write(buf);
        if self.splitter.len() == 0 {
            return true;
        }

        let mut buffer = self.buffer.write().await;
        while let Some(frame) = self.splitter.take_frame() {
            buffer.push(frame);
        }
        let latest_pos = buffer.latest_pos();
        drop(buffer);

        self.notify_waiting(latest_pos).await
    }

    async fn notify_waiting(&self, latest_pos: u64) -> bool {
        let waiting = match self.waiting.upgrade() {
            Some(w) => w,
            None => return false,
        };
        let mut waiting = waiting.lock().await;

        while let Some(entry) = waiting.first_entry() {
            if *entry.key() > latest_pos {
                break;
            }

            let (_, senders) = entry.remove_entry();
            for sender in senders {
                let _ = sender.send(());
                // canceled: client went away, ok whatever
                // would be nice if we could arrange for clients that go away
                // to remove themselves from the list but idk how that works
            }
        }

        return true;
    }
}

impl Rx {
    pub async fn get_frame_from(&self, pos: u64) -> Option<(Bytes, u64)> {
        loop {
            let rx = {
                let mut waiting = {
                    let buffer = self.buffer.upgrade()?;
                    let buffer = buffer.read().await;
                    if let Some(r) = buffer.get_from_pos(pos) {
                        return Some(r);
                    }
                    // hold on to buffer lock until we have the waiters lock,
                    // so we can't race and miss being notified

                    self.waiting.lock().await
                };

                let (tx, rx) = oneshot::channel();
                waiting.entry(pos).or_default().push(tx);
                rx
            }; // can't hold lock while waiting

            if rx.await.is_err() {
                return None; // stream went away
            } // else retry!
        }
    }
}

struct MjpegSplitter {
    buf: Vec<u8>,
    scanned: usize,
}

impl MjpegSplitter {
    fn new() -> MjpegSplitter {
        MjpegSplitter {
            buf: Vec::new(),
            scanned: 0,
        }
    }

    fn write(&mut self, buf: &[u8]) {
        self.buf.extend_from_slice(buf);
    }

    fn len(&self) -> usize {
        return self.buf.len();
    }

    fn take_frame(&mut self) -> Option<bytes::Bytes> {
        while self.scanned + 3 < self.buf.len() {
            if &self.buf[self.scanned..self.scanned+4]
                    == &[0xff, 0xd9, 0xff, 0xd8] {
                self.scanned += 2;
                let result =
                    bytes::Bytes::copy_from_slice(&self.buf[..self.scanned]);
                self.buf.drain(..self.scanned);
                self.scanned = 2;
                return Some(result);
            }
            self.scanned += 1;
        }

        return None;
    }
}

#[derive(Debug, Default)]
struct Buffer {
    pos_offset: u64,
    max_frames: usize,
    frames: VecDeque<Bytes>,
}

impl Buffer {
    fn new() -> Buffer {
        Buffer {
            pos_offset: 0,
            max_frames: 15 * 30, // 15s; todo: vary on fps
            frames: VecDeque::new(),
        }
    }

    fn latest_pos(&self) -> u64 {
        return self.pos_offset + self.frames.len() as u64;
    }

    fn push(&mut self, frame: Bytes) {
        self.frames.push_back(frame);

        if self.frames.len() > self.max_frames {
            let excess = self.frames.len() - self.max_frames;
            self.frames.drain(..excess);
            self.pos_offset += excess as u64;
        }
    }

    fn get_from_pos(&self, pos: u64) -> Option<(Bytes, u64)> {
        let (i, pos) = if pos == 0 && self.frames.len() > 0 {
            // caller just showed up, just give them the latest frames
            // right away so they dont try to catch up through our full
            // buffer
            //
            // this seems a bit ugly, think harder about this?
            (self.frames.len() - 1,
                self.pos_offset + self.frames.len() as u64 - 1)
        } else if pos < self.pos_offset && self.frames.len() > 0 {
            // caller is way behind, rip
            (0, self.pos_offset)
        } else if pos < self.pos_offset + self.frames.len() as u64 {
            // the good branch
            ((pos - self.pos_offset) as usize, pos)
        } else {
            // "buffer empty", nothing to do but wait for the next frame
            return None;
        };

        return Some((self.frames[i].clone(), pos));
    }
}
