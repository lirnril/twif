use std::collections::BTreeMap;
use std::str::FromStr;

use eyre::Result;
use spandoc::spandoc;
use tracing::instrument;
use tracing::info;
use tracing::debug;
use url::Url;

use super::Channel;

//pub struct Manifest {
//    pub m3u8: hls_m3u8::MasterPlaylist,
//    pub renditions:
//    // ... extra twitch bits?
//}

#[derive(PartialEq, PartialOrd, Eq, Ord, Clone, Debug)]
pub struct Rendition {
    pub playlist_url: String,
    pub fps: u32,
    pub width: u32,
    pub height: u32,
}

#[derive(Debug)]
pub struct Requester {
    client: reqwest::Client,
}

impl Requester {
    #[instrument]
    pub fn new() -> Requester {
        let client = reqwest::Client::new();
        Requester { client }
    }

    #[spandoc]
    #[instrument]
    pub async fn get_renditions(&self, channel: &Channel)
            -> Result<BTreeMap<u32, Rendition>> {
        let manifest = self.get_manifest(channel).await?;
        let renditions = renditions_by_vertical_resolution(&manifest);
        if renditions.len() == 0 {
            Err(eyre::eyre!("no rendition available"))?;
        }

        info!("got renditions for channel");

        Ok(renditions)
    }

    //#[spandoc]
    #[instrument]
    pub async fn get_manifest(&self, channel: &Channel)
            -> Result<hls_m3u8::MasterPlaylist> {
        debug!("fetching renditions for channel");

        let manifest_url = self.get_manifest_url(channel).await?;

        //// SPANDOC: Making manifest request
        let result = self.client.get(manifest_url)
            .send().await?.error_for_status()?;
        //// SPANDOC: Reading manifest
        let manifest_response = result.text().await?;
        debug!(?manifest_response, "retrieved master manifest");
        //// SPANDOC: Parsing manifest
        Ok(hls_m3u8::MasterPlaylist::from_str(&manifest_response)?)
    }

    //#[spandoc]
    #[instrument]
    async fn get_manifest_url(&self, channel: &Channel) -> Result<Url> {
        let r = match channel {
            Channel::Twitch { channel } => {
                //// SPANDOC: Requesting access token
                let body = String::from(r#"{"operationName":"PlaybackAccessToken_Template","query":"query PlaybackAccessToken_Template($login: String!, $isLive: Boolean!, $vodID: ID!, $isVod: Boolean!, $playerType: String!) {  streamPlaybackAccessToken(channelName: $login, params: {platform: \"web\", playerBackend: \"mediaplayer\", playerType: $playerType}) @include(if: $isLive) {    value    signature    __typename  }  videoPlaybackAccessToken(id: $vodID, params: {platform: \"web\", playerBackend: \"mediaplayer\", playerType: $playerType}) @include(if: $isVod) {    value    signature    __typename  }}","variables":{"isLive":true,"login":""#) + channel + r#"","isVod":false,"vodID":"","playerType":"site"}}"#;
                let result = self.client.post("https://gql.twitch.tv/gql")
                    .header("Client-Id", "jzkbprff40iqj646a697cyrvl0zt2m6")
                    .body(body)
                    .send()
                    .await?
                    .error_for_status()?;
                //// SPANDOC: Parsing access token json
                let access_token_response: twitch::GqlAccessTokenResponse
                    = result.json().await?;
                let access_token = access_token_response
                    .data.stream_playback_access_token;

                debug!(?access_token, "retrieved access token");
                twitch::manifest_url(channel,
                    &access_token.signature, &access_token.value)
            },
            Channel::Ivs { endpoint, region, customer, channel } => {
                Url::parse(&format!(
                    "https://{endpoint}.{region}.playback.live-video.net\
                        /api/video/v1/\
                        {region}.{customer}.channel.{channel}.m3u8",
                    endpoint = endpoint,
                    region = region,
                    customer = customer,
                    channel = channel))?
            }
        };

        Ok(r)
    }
}

mod twitch {
    use super::*;

    #[instrument]
    pub fn access_token_url(channel_name: &str) -> Url {
        let mut u = Url::parse("https://api.twitch.tv/api/channels").unwrap();
        u.path_segments_mut().unwrap()
            .push(channel_name)
            .push("access_token");
        debug!(url = %u, "constructing access token url");
        u
    }

    #[derive(Debug, serde::Serialize, serde::Deserialize)]
    pub struct AccessTokenResponse {
        pub token: String,
        pub sig: String,
    }

    #[derive(Debug, serde::Serialize, serde::Deserialize)]
    pub struct GqlAccessTokenResponse {
        pub data: GqlAccessTokenResponseData,
    }

    #[derive(Debug, serde::Serialize, serde::Deserialize)]
    pub struct GqlAccessTokenResponseData {
        #[serde(rename = "streamPlaybackAccessToken")]
        pub stream_playback_access_token: GqlAccessTokenResponseDataToken,
    }

    #[derive(Debug, serde::Serialize, serde::Deserialize)]
    pub struct GqlAccessTokenResponseDataToken {
        pub value: String,
        pub signature: String,
    }

    #[instrument]
    pub fn manifest_url(channel_name: &str, sig: &str, token: &str) -> Url {
        // TODO: downcase channel_name before here
        let mut u = Url::parse_with_params(
            "https://usher.ttvnw.net/api/channel/hls",
            &[("sig", sig), ("token", token)]).unwrap();
        u.path_segments_mut().unwrap()
            .push(&format!("{}.m3u8", channel_name));
        debug!(url = %u, "constructing twitch manifest url");
        u
    }
}

#[instrument]
fn renditions_by_vertical_resolution(manifest: &hls_m3u8::MasterPlaylist)
        -> BTreeMap<u32, Rendition> {
    manifest.video_streams().filter_map(|s| {
        use hls_m3u8::tags::VariantStream::ExtXStreamInf;
        let (playlist_url, fps) = match s {
            ExtXStreamInf { uri, frame_rate, .. } => (uri, frame_rate),
            _ => return None,
        };
        debug!(resolution = ?s.resolution(), "has rendition");
        let (width, height) = s.resolution()
            .map(|r| (r.width() as u32, r.height() as u32))
            .unwrap_or((0, 0));
        if width == 0 || height == 0 {
            return None;
        }

        let playlist_url = playlist_url.to_string();
        let fps = fps.map(|u| u.as_f32() as u32).unwrap_or(0);

        Some((height, Rendition { playlist_url, fps, width, height }))
    }).collect()
}
