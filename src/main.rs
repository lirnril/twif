extern crate twif;

use {
    std::{
        collections::BTreeMap,
        io,
        net,
        str::FromStr,
        sync::Arc,
        future::Future,
        time::Duration,
    },

    eyre::{Result, WrapErr},
    tracing::{info, info_span, instrument},
    tracing_futures::Instrument,
    spandoc::spandoc,
    tokio::{
        time,
        sync::Mutex,
    },

    twif::{stream, manifest, Channel},
};

#[derive(Debug, structopt::StructOpt)]
struct Args {
    #[structopt(long = "listen-addr", name = "ADDR", default_value = "127.0.0.1:8080")]
    listen_addr: net::SocketAddr,
}

struct Rendition {
    rendition: manifest::Rendition,
    playing: Option<stream::Fork>,
}

struct App {
    active_streams: Mutex<BTreeMap<Channel, BTreeMap<u32, Rendition>>>,
    manifest_requester: manifest::Requester,
}

type Request = hyper::Request<hyper::Body>;
type Response = hyper::Response<hyper::Body>;

#[paw::main]
fn main(args: Args) -> Result<()> {
    init_tracing()?;

    let app = Arc::new(App {
        active_streams: Mutex::new(BTreeMap::new()),
        manifest_requester: manifest::Requester::new(),
    });

    let runtime = tokio::runtime::Runtime::new()?;
    runtime.block_on(async move {
        tokio::spawn({
            let app = app.clone();
            async move {
                let mut interval
                    = tokio::time::interval(Duration::from_secs(30));
                loop {
                    interval.tick().await;
                    print_state(&app).await;
                }
            }
        });

        run_hyper_service(&args.listen_addr, move |req| async move {
            if let Some(response) = handle_static_response(&req) {
                return Ok(response);
            }

            handle_stream_request(app.clone(), req).await
        }).await
    })?;


    Ok(())
}

#[spandoc]
fn init_tracing() -> Result<()> {
    use tracing_subscriber::{Registry, EnvFilter, layer::SubscriberExt};

    let filter = EnvFilter::try_from_default_env()
        .or_else(|_| EnvFilter::try_new("info"))
        .unwrap();

    let fmt = tracing_subscriber::fmt::Layer::new()
        .with_writer(io::stderr);
    let subscriber = Registry::default()
        .with(tracing_error::ErrorLayer::default())
        .with(filter)
        .with(fmt);

    /// SPANDOC: Setting up default tracing subscriber
    tracing::subscriber::set_global_default(subscriber)?;

    Ok(())
}

async fn run_hyper_service<F, R>(listen_addr: &net::SocketAddr,
            handle_request: F) -> Result<()>
        where F: FnOnce(Request) -> R + Send + 'static + Clone,
            R: Future<Output = Result<Response>> + Send + 'static {
    use hyper::service::{make_service_fn, service_fn};

    let make_svc = make_service_fn(move |_| {
        let handle_request = handle_request.clone();
        async move {
            let handle_request = handle_request.clone();
            Ok::<_, eyre::Report>(service_fn(move |req: Request| {
                handle_request.clone()(req)
            }))
        }
    });

    hyper::server::Server::try_bind(listen_addr)?
        .serve(make_svc).await?;

    Ok(())
}

fn get_target_from_path(mut path: &str) -> (Channel, u32) {
    if path.starts_with("/ivs/") {
        let path = path.strip_prefix("/ivs/").unwrap();
        let mut s = path.split('/');
        let endpoint = s.next().unwrap_or("").to_owned();
        let region = s.next().unwrap_or("").to_owned();
        let customer = s.next().unwrap_or("").to_owned();
        let channel = s.next().unwrap_or("").to_owned();

        let target = Channel::Ivs {
            endpoint, region, customer, channel
        };

        let minimum_resolution = s.next()
            .filter(|x| !x.is_empty())
            .and_then(|s| u32::from_str(s).ok())
            .unwrap_or(0);

        (target, minimum_resolution)
    } else {
        if path.starts_with("/twitch/") {
            path = path.strip_prefix("/twitch").unwrap();
        } else if path == "/twitch" {
            path = "/";
        }

        let mut segments = path.split('/').skip(1);
        let channel_name = segments.next()
            .filter(|x| !x.is_empty())
            .unwrap_or("twitchmedia1");

        let target = Channel::Twitch {
            channel: channel_name.to_owned()
        };

        let minimum_resolution = segments.next()
            .filter(|x| !x.is_empty())
            .and_then(|s| u32::from_str(s).ok())
            .unwrap_or(0);
        // ^ ugh duplication
        (target, minimum_resolution)
    }
}

fn handle_static_response(req: &Request) -> Option<Response> {
    match req.uri().path() {
        "/health" => Some(Response::new(hyper::Body::empty())),
        // todo: 404?
        "/favicon.ico" => Some(Response::new(hyper::Body::empty())),
        _ => None
    }
}

async fn handle_stream_request(app: Arc<App>, req: Request)
        -> Result<Response> {
    let path = req.uri().path();

    let (target, minimum_resolution) = get_target_from_path(path);

    let header = |name| req.headers()
        .get(name)
        .and_then(|v|v.to_str().ok())
        .unwrap_or("")
        .to_owned();

    // TODO: block chime on iOS b/c it crashes, "Chime/4.32.8397 CFNetwork/1128.0.1 Darwin/19.6.0"
    let user_agent = header("User-agent");

    if user_agent.contains(" Discordbot/2.0;") {
        if let Channel::Twitch { channel } = target {
        return Ok(hyper::Response::builder()
            .status(hyper::StatusCode::SEE_OTHER)
            .header("Location", format!("https://twitch.tv/{}", channel))
            .body(hyper::Body::empty())?);
        }
    }

    let referrer = header("Referer");
    let client_ip = header("X-forwarded-for");

    let serve = serve_stream(target.clone(),
            minimum_resolution, app.clone());
    let serve = serve.instrument(
        info_span!("serve_stream",
            ?target, ?minimum_resolution,
            ?user_agent, ?referrer, ?client_ip));

    Ok::<_, eyre::Report>(match serve.await {
        Ok(response) => response,
        Err(e) => {
            info!(?e, "unhappy serve_stream exit");
            Response::new(hyper::Body::from(e.to_string()))
        }
    })
}

async fn serve_stream(channel: Channel, minimum_resolution: u32,
            app: Arc<App>)
        -> Result<Response> {
    let mut a = app.active_streams.lock().await;
    let mut a_;
    let renditions = match a.get_mut(&channel) {
        Some(renditions) => {
            info!("already have renditions");
            renditions
        },
        None => {
            drop(a);
            let renditions =
                app.manifest_requester.get_renditions(&channel).await
                    .wrap_err_with(||
                        format!("couldn't fetch renditions for channel {:?}",
                            channel))?;
            let renditions =
                renditions.into_iter().map(|(resolution, rendition)| {
                    let playing = None;
                    (resolution, Rendition { rendition, playing })
                }).collect();
            a_ = app.active_streams.lock().await;
            let renditions = a_.entry(channel.clone())
                .or_insert(renditions);
            renditions
        },
    };
    info!("got rendition");

    // TODO; what a pain, renditions should just be a vec
    let preferred = renditions.iter()
        .map(&|(&h, _)| h)
        .find(|&h| h >= minimum_resolution);
    let vertical_resolution = match preferred {
        Some(h) => h,
        None => *renditions.last_key_value().unwrap().0,
    };
    let rendition = renditions.get_mut(&vertical_resolution).unwrap();

    let rx = match rendition.playing.as_ref().and_then(|f| f.get_rx()) {
        Some(rx) => rx,
        None => {
            let (rx, fork) =
                start_stream(channel, &rendition, app.clone()).await
                        .wrap_err("couldn't start stream")?;
            rendition.playing = Some(fork);
            rx
        }
    };

    let (sender, body) = hyper::Body::channel();

    tokio::task::spawn(async {
        match consume_stream(rx, sender).await {
            Ok(()) => Ok(()),
            Err(e) => {
                eprintln!("{:#?}", e);
                info!(?e, "unhappy consume_stream exit");
                Err(e)
            }
        }
    });

    let response = hyper::Response::builder()
       .header("Content-Type", "multipart/x-mixed-replace;boundary=lb9dyP9EyP1lfuVo1ZvEQh86ZAT4Ki")
       .body(body)?;

    Ok(response)
}

#[instrument(skip(rendition, app))]
async fn start_stream(channel: Channel, rendition: &Rendition, app: Arc<App>)
        -> Result<(stream::Rx, stream::Fork)> {
    // TODO; can we leave the serve_stream span here?
    use tokio::process::Command;
    use std::process::Stdio;
    let mut child_1 = Command::new("ffmpeg")
        .args(&[
            "-loglevel", "error",
            "-re",
            "-i", &rendition.rendition.playlist_url,
            "-c", "copy",
            "-f", "flv",
            "-"])
        .stdout(Stdio::piped())
        .kill_on_drop(true)
        .spawn()
        .wrap_err("couldn't launch first ffmpeg")?;
    let mut child_2 = Command::new("ffmpeg")
        .args(&[
            "-loglevel", "error",
            "-i", "-",
            // twitch doesn't always serve 360p video for the 360p
            // rendition. Sometimes it serves ads that are higher
            // resolution. I don't think we want to switch resolutions
            // between ads and live content, so let's ask ffmpeg to
            // normalize them.
            "-vf", &format!("scale={}:{}",
                rendition.rendition.width, rendition.rendition.height),
            "-vcodec", "mjpeg",
            "-f", "mjpeg",
            "-"])
        .stdin(child_1.stdout.take().unwrap().into_owned_fd().unwrap())
        .stdout(Stdio::piped())
        .kill_on_drop(true)
        .spawn()
        .wrap_err("couldn't launch second ffmpeg")?;

    let vertical_resolution = rendition.rendition.height;

    let mut child_output = child_2.stdout.take().unwrap();

    let (mut tx, rx, fork) = stream::new();

    let consume_ffmpeg = async move {
        use tokio::io::AsyncReadExt;
        let mut buf = [0u8; 8*1024];

        loop {
            let n = child_output.read(&mut buf).await
                .wrap_err("couldn't read from ffmpeg")?;
            if n == 0 {
                break;
            }

            if !tx.write(&buf[..n]).await {
                //no more clients listening
                break;
            }
        }

        Ok::<_, eyre::ErrReport>(())
    };

    tokio::task::spawn(async move {
        tokio::pin!(consume_ffmpeg);
        tokio::select! {
            _ = &mut consume_ffmpeg => {
                info!("consume_ffmpeg done");
                child_1.kill().await
                    .wrap_err("couldn't kill first ffmpeg")?;
                child_1.wait().await
                    .wrap_err("couldn't wait for first ffmpeg exit")?;
                child_2.kill().await
                    .wrap_err("couldn't kill second first ffmpeg")?;
                child_2.wait().await
                    .wrap_err("couldn't wait for second ffmpeg exit")?;
            },
            _ = child_1.wait() => {
                info!("first ffmpeg exited");
                // I don't think we do anything here?
            },
            _ = child_2.wait() => {
                info!("second ffmpeg exited");
                time::timeout(Duration::from_secs(3), async {
                    // terminates because child's stdout is closed
                    consume_ffmpeg.await
                        .wrap_err("consume_ffmpeg failed")?;
                    info!("awaited consume_ffmpeg");
                    Ok::<_, eyre::ErrReport>(())
                }).await??;
            },
        }
        let mut a = app.active_streams.lock().await;
        // TODO: this unwrap panicked. whyyy.
        let renditions =
            a.get_mut(&channel).unwrap();
        renditions.get_mut(&vertical_resolution).unwrap().playing = None;
        if renditions.iter().all(|r| r.1.playing.is_none()) {
            a.remove(&channel);
        }
        info!("stream done!");
        Ok::<_, eyre::ErrReport>(())
    });

    Ok((rx, fork))
}

async fn consume_stream(rx: stream::Rx, mut sender: hyper::body::Sender)
        -> Result<()> {
    let mut pos = 0;

    while let Some((frame, new_pos)) = rx.get_frame_from(pos).await {
        pos = new_pos + 1;

        // TODO: non-30 fps
        // basically we want to delay a tiny bit less than the natural
        // framerate so that when we're behind, we catch up, but not
        // too fast. maybe we should do this based on pos / in rx.
        let delay = time::sleep(Duration::from_millis(1000 / 38));

        let header = format!("\r\n\
            --lb9dyP9EyP1lfuVo1ZvEQh86ZAT4Ki\r\n\
            Content-type: image/jpeg\r\n\
            Content-length: {}\r\n\
            \r\n\
            ", frame.len());
        // try to be lenient here, during shutdown we time out
        // earlier anyway
        let r = time::timeout(Duration::from_secs(120), async {
                    sender.send_data(header.into()).await
                        .wrap_err("couldn't send header")?;
                    sender.send_data(frame).await
                        .wrap_err("couldn't send data")?;
                    Ok::<_, eyre::ErrReport>(())
                }).await
            .wrap_err("couldn't timeout")?;
        if let Err(e) = r {
            let h: Option<&hyper::Error> = e.downcast_ref();
            if h.is_some() {
                info!(?e, "hyper error, client probably went away");
                return Ok(());
            } else {
                info!(?e, "error while sending");
                eyre::bail!(e);
            }
        }

        delay.await;
    }
    info!("out of frames");

    Ok::<_, eyre::ErrReport>(())
}

async fn print_state(app: &App) {
    let mut active_streams: BTreeMap<&Channel, BTreeMap<u32, usize>> = BTreeMap::new();
    let mut total_viewers = 0;
    let streams = app.active_streams.lock().await;

    for (channel, renditions) in &*streams {
        let mut clients_per_rendition = BTreeMap::new();
        for (&height, rendition) in renditions {
            let Some(playing) = &rendition.playing else { continue; };
            let count = playing.get_count();
            total_viewers += count;
            clients_per_rendition.insert(height, playing.get_count());
        }
        if clients_per_rendition.len() > 0 {
            active_streams.insert(channel, clients_per_rendition);
        }
    }
    if active_streams.len() > 0 {
        info!(?active_streams, ?total_viewers, "currently active streams");
    }
}
