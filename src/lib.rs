use std::fmt;

pub mod manifest;
pub mod stream;

#[derive(PartialEq, PartialOrd, Eq, Ord, Clone)]
pub enum Channel {
    Twitch {
        channel: String,
    },
    Ivs {
        endpoint: String,
        region: String,
        customer: String,
        channel: String,
    },
}

impl fmt::Debug for Channel {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Channel::Twitch { channel } => write!(f, "{}", channel),
            Channel::Ivs { endpoint, region, customer, channel } =>
                write!(f, "ivs.{}.{}.{}.{}", region, customer, channel, endpoint),
        }
    }
}
