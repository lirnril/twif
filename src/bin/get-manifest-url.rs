use std::collections::BTreeMap;
use structopt::StructOpt as _;
use twif::Channel;
use twif::manifest::Requester;
use eyre::Result;

#[derive(Debug, structopt::StructOpt)]
struct Args {
    #[structopt(name = "CHANNEL", default_value = "twitchmedia28")]
    channel: String,
    #[structopt(short, long = "vertical-resolution")]
    vertical_resolution: Option<u32>,
}
#[tokio::main]
async fn main() -> Result<()> {
    let Args { channel, vertical_resolution }  = Args::from_args();
    let r = Requester::new();
    let renditions = r.get_renditions(&Channel::Twitch { channel }).await?;

    match vertical_resolution {
        Some(vertical_resolution) => print_rendition(&renditions, vertical_resolution),
        None => print_all_renditions(&renditions),
    }

    Ok(())
}

fn print_all_renditions(renditions: &BTreeMap<u32, twif::manifest::Rendition>) {
    for (_, rendition) in renditions {
        println!("{}x{} at {} fps: {}", rendition.width, rendition.height, rendition.fps, rendition.playlist_url);
    }
}

fn print_rendition(renditions: &BTreeMap<u32, twif::manifest::Rendition>, rendition: u32) {
    let preferred = renditions.iter()
        .map(&|(&h, _)| h)
        .find(|&h| h >= rendition);

    let vertical_resolution = match preferred {
        Some(h) => h,
        None => *renditions.last_key_value().unwrap().0,
    };

    let rendition = renditions.get(&vertical_resolution).unwrap();

    eprintln!("using rendition {}x{} at {} fps", rendition.width, rendition.height, rendition.fps);
    println!("{}", rendition.playlist_url);
}
